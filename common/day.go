package common

type Day interface {
	Run()
	GetInput() string
}