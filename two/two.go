package two

import (
	"aoc_twentytwentyone/common"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type Two struct {
	common.Day
}

const (
	FORWARD = "forward"
	UP = "up"
	DOWN = "down"
)

func (day Two) Run() {
	input := day.GetInput()

	var cptOrders []order

	// split it into an array broken by \n
	data := strings.Split(input, "\n")

	// use backticks you sausage
	regex := regexp.MustCompile(`(?P<direction>\w+)\s+(?P<amount>\d+)`)

	for _, thisItemStr := range data {
		if len(strings.TrimSpace(thisItemStr)) == 0 {
			break
		}

		// parse the direction and amount
		matches := regex.FindAllSubmatch([]byte(thisItemStr), -1)

		direction := string(matches[0][1])
		amount, _ := strconv.Atoi(string(matches[0][2]))

		cptOrders = append(cptOrders, order{
			Direction: direction,
			Amount: amount,
		})
	}

	log.Printf("day 2 - part 1: %d", day.partOne(cptOrders))
	log.Printf("day 2 - part 2: %d", day.partTwo(cptOrders))
}

func (day Two) partOne(orders []order) int {
	/**
	  forward X increases the horizontal position by X units.
	  down X increases the depth by X units.
	  up X decreases the depth by X units.
	*/

	horizontalPosition := 0
	depthPosition := 0

	for _, cptOrder := range orders {
		switch cptOrder.Direction {
		case FORWARD:
			horizontalPosition += cptOrder.Amount
			break
		case UP:
			depthPosition -= cptOrder.Amount
			break
		case DOWN:
			depthPosition += cptOrder.Amount
		default:
			log.Panic(errors.New(fmt.Sprintf("Unexpected direction: %s", cptOrder.Direction)))
		}
	}

	return horizontalPosition * depthPosition
}

func (day Two) partTwo(orders []order) int {
	/**
	  down X increases your aim by X units.
	  up X decreases your aim by X units.
	  forward X does two things:
	      It increases your horizontal position by X units.
	      It increases your depth by your aim multiplied by X.
	*/

	horizontalPosition := 0
	depthPosition := 0
	aim := 0

	for _, cptOrder := range orders {
		switch cptOrder.Direction {
		case FORWARD:
			horizontalPosition += cptOrder.Amount
			depthPosition += aim * cptOrder.Amount
			break
		case UP:
			aim -= cptOrder.Amount
			break
		case DOWN:
			aim += cptOrder.Amount
		default:
			log.Panic(errors.New(fmt.Sprintf("Unexpected direction: %s", cptOrder.Direction)))
		}
	}

	return horizontalPosition * depthPosition
}

func (day Two) GetInput() string {
	rawData, err := ioutil.ReadFile("two/input.txt")
	if err != nil {
		log.Panic(err)
	}

	return string(rawData)
}

type order struct {
	Direction string
	Amount int
}