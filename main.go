package main

import (
	"aoc_twentytwentyone/common"
	"aoc_twentytwentyone/one"
	"aoc_twentytwentyone/three"
	"aoc_twentytwentyone/two"
	"flag"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

func main() {
	debugMode := false
	specificDaysStr := ""

	flag.BoolVar(&debugMode, "debug", false, "set when you want to debug")
	flag.StringVar(&specificDaysStr, "days", "", "comma separated specific days to run")
	flag.Parse()

	log.SetLevel(log.InfoLevel)
	if debugMode {
		log.SetLevel(log.DebugLevel)
	}

	days := []common.Day{
		one.One{},
		two.Two{},
		three.Three{},
	}

	specificDaysStrArr := strings.Split(specificDaysStr, ",")
	specificDays := []int{}

	for _, specificDayStr := range specificDaysStrArr {
		specificDay, _ := strconv.Atoi(specificDayStr)
		specificDays = append(specificDays, specificDay)
	}

	shouldCheckForSpecificDays := len(specificDays) > 0

	for dayKey, day := range days {
		if shouldCheckForSpecificDays {
			for _, specificDay := range specificDays {
				if specificDay != dayKey {
					continue
				}
			}
		}

		day.Run()
	}
}