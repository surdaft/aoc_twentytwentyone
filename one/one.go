package one

import (
	"aoc_twentytwentyone/common"
	log "github.com/sirupsen/logrus"

	"io/ioutil"
	"strconv"
	"strings"
)

type One struct {
	common.Day
}

//Run today
func (day One) Run() {
	input := day.GetInput()

	var parsedInput []int

	// split it into an array broken by \n
	data := strings.Split(input, "\n")

	for _, thisItemStr := range data {
		thisItem, _ := strconv.Atoi(thisItemStr)
		parsedInput = append(parsedInput, thisItem)
	}

	log.Printf("day 1 - part 1: %d", day.partOne(parsedInput))
	log.Printf("day 1 - part 2: %d", day.partTwo(parsedInput))
}

// partOne return the answer for part 1
func (day One) partOne(data []int) int {
	var lastItem int = -1
	var increases int = 0
	for _, thisItem := range data {
		if lastItem > 0 && thisItem > lastItem {
			increases++
		}

		lastItem = thisItem
	}

	return increases
}

// partTwo this one got me, I overcomplicate things sometimes
// I was thinking lets create a window, then do some stuff to
// then move the window across, and create a debug thing that
// recreates their example... It took me far too long to
// identify that it's just this plus the 2 next items everytime
func (day One) partTwo(data []int) int {
	var windows []int
	dataLen := len(data)

	for k, thisItem := range data {
		// it's no longer possible to create a window of 3
		if k >= dataLen - 2 {
			break
		}

		nextItem := data[k + 1]
		nextNextItem := data[k + 2]

		// add this and the next 2 items together, for the window of 3
		windows = append(windows, thisItem + nextItem + nextNextItem)
	}

	lastDepth := 0
	depthIncreaseCount := 0

	for k, depth := range windows {
		// first one, so set the depth so it can compare but to itself
		// so its not an increase
		if k == 0 {
			lastDepth = depth
		}

		if depth > lastDepth {
			depthIncreaseCount++
		}

		// update the depth
		lastDepth = depth
	}

	return depthIncreaseCount
}

// GetInput return the puzzle input for today
func (day One) GetInput() string {
	rawData, err := ioutil.ReadFile("one/input.txt")
	if err != nil {
		log.Panic(err)
	}

	return string(rawData)
}