package three

import (
	"aoc_twentytwentyone/common"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type Three struct {
	common.Day
}

func (day Three) Run() {
	input := day.GetInput()

	var parsedInput []string

	// split it into an array broken by \n
	data := strings.Split(input, "\n")

	for _, thisItemStr := range data {
		if len(thisItemStr) == 0 {
			continue
		}

		parsedInput = append(parsedInput, thisItemStr)
	}

	log.Printf("day 3 - part 1: %d", day.partOne(parsedInput))
	log.Printf("day 3 - part 2: %d", day.partTwo(parsedInput))
}

func (day Three) partOne(input []string) int {
	// gamma rate
	// epsilon rate

	// gamma * epsilon
	// gamma is most common value in the column
	// epsilon is least common value in column

	countColIncr := make(map[int]map[int]int)
	bitParts := make(map[string][]string)

	// if we reach a count in this col that is higher than
	// this, then it's not possible to recover
	pointNoReturn := int(math.Ceil(float64(len(input)) / 2))

	for i := 0; i < len(input[0]); i++ {
		countColIncr[i] = make(map[int]int, 2)

		countColIncr[i][0] = 0
		countColIncr[i][1] = 0

		bitParts["gamma"] = append(bitParts["gamma"], "")
		bitParts["epsilon"] = append(bitParts["epsilon"], "")
	}

	for _, rowValues := range input {
		columns := strings.Split(rowValues, "")
		wins := 0

		for columnKey, columnValueRaw := range columns {
			zeroWins := countColIncr[columnKey][0] > pointNoReturn
			oneWins := countColIncr[columnKey][1] > pointNoReturn

			// skip this column, no longer need to care
			if zeroWins || oneWins {
				wins++
				continue
			}

			columnValue, _ := strconv.Atoi(columnValueRaw)
			countColIncr[columnKey][columnValue]++

			zeroWins = countColIncr[columnKey][0] > pointNoReturn
			oneWins = countColIncr[columnKey][1] > pointNoReturn

			if zeroWins {
				bitParts["gamma"][columnKey] = "0"
				bitParts["epsilon"][columnKey] = "1"
				log.Debugf("column entered point of no return: %d %d", columnKey, 0)
			} else if oneWins {
				bitParts["gamma"][columnKey] = "1"
				bitParts["epsilon"][columnKey] = "0"
				log.Debugf("column entered point of no return: %d %d", columnKey, 1)
			}
		}

		// processed enough of the input
		if wins >= len(input[0]) {
			break
		}
	}

	gamma := strings.Join(bitParts["gamma"], "")
	gammaInt, _ := strconv.ParseInt(gamma, 2, 64)

	epsilon := strings.Join(bitParts["epsilon"], "")
	epsilonInt, _ := strconv.ParseInt(epsilon, 2, 64)

	return int(gammaInt * epsilonInt)
}

func (day Three) partTwo(input []string) int {
	// get life support
	// multiply oxygen gen with co2 scrubber

	// clone value, but this will be replaced on each iteration
	oxygenInput := input
	coInput := input

	oxygen := ""
	co := ""

	// go through each column
	for i := 0; i <= len(input[0]); i++ {
		log.Debugf("oxygen has %d values left", len(oxygenInput))
		log.Debugf("co2 has %d values left", len(coInput))

		if oxygen == "" {
			oxygenInput = day.partTwoSortValues(oxygenInput, i, true)
		}

		if co == "" {
			coInput = day.partTwoSortValues(coInput, i, false)
		}

		if len(oxygenInput) == 1 {
			oxygen = oxygenInput[0]
		}

		if len(coInput) == 1 {
			co = coInput[0]
		}
	}

	log.Debugf("oxygen: %s", oxygen)
	log.Debugf("co: %s", co)

	oxygenInt, _ := strconv.ParseInt(oxygen, 2, 64)
	coInt, _ := strconv.ParseInt(co, 2, 64)

	return int(oxygenInt * coInt)
}

func (day Three) partTwoSortValues(input []string, column int, win bool) []string {
	log.Debugf("sorting %d %d", len(input), column)

	colIncr := make(map[string]int)

	colIncr["0"] = 0
	colIncr["1"] = 0

	// determine the most popular for this column
	for _, str := range input {
		// pull out the column
		columnValue := str[column:column + 1]

		if len(input) < 50 {
			log.Debugf("%s[%d:%d] := %s", str, column, column+1, columnValue)
		}

		colIncr[columnValue]++
	}

	newInput := []string{}

	// specifically the 1 is most popular until proven otherwise
	// if equal 1 takes preference
	// this tripped me up
	mostPopular := "1"
	leastPopular := "0"
	if colIncr["0"] > colIncr["1"] {
		mostPopular = "0"
		leastPopular = "1"
	}

	log.WithFields(log.Fields{
		"mostPopular": mostPopular,
		"leastPopular": leastPopular,
	}).Debugf("%d > %d", colIncr["1"], colIncr["0"])

	toMatch := mostPopular
	if !win {
		toMatch = leastPopular
	}

	// ok cool, now we have the most popular, lets build the new array of those items
	for _, str := range input {
		if str[column:column + 1] == toMatch {
			newInput = append(newInput, str)
		}
	}

	return newInput
}

func (day Three) GetInput() string {
	rawData, err := ioutil.ReadFile("three/input.txt")
	if err != nil {
		log.Panic(err)
	}

	return string(rawData)
}
